const http = require('http');
const express = require('express');
const app = express();
const bodyParser = require('body-parser');

app.use(bodyParser.json({ limit : '200mb' })) // for parsing application/json
app.use(bodyParser.urlencoded({ limit: '200mb', extended: true })) // for parsing application/x-www-form-urlencoded


const hostname = '127.0.0.1';
const port = process.env.PORT || 3000;
app.listen(port,  console.log(`Server running at http://${hostname}:${port}/`));

//direct to api routing
app.use('/api',require('./routes/apis'));
//direct to web routing
app.use('/',require('./routes/web'));

module.exports = app;