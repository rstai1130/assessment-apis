'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    var user_arr = [
      /*{
        teacherid : 23,
        studentid : 29,
      },        
      {
        teacherid : 23,
        studentid : 33,
      }, */      
    ];

    var finalized = user_arr.map((user)=> {
      user.createdAt = new Date() ;
      user.updatedAt = new Date() ;
      return user;
    })

    /*crypto.randomBytes(16, (err, buf) => {
      if (err) throw err;
      console.log(`${buf.length} bytes of random data: ${buf.toString('hex')}`);
    });*/
    console.log(finalized);
    return queryInterface.bulkInsert('Registers',finalized);
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Registers', null, {});
  }
};
