'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {

    var user_arr = [
      {
        email: 'teacherken@gmail.com',
        api_keys : JSON.stringify([ { teacher : 'some_hashed_salted_general_keys' }]) ,
      },       
      {
        email: 'teacherjoe@gmail.com',
        api_keys : JSON.stringify([ { teacher : 'some_hashed_salted_general_keys' }]) ,
      },      
    ];

    var finalized = user_arr.map((user)=> {
      user.createdAt = new Date() ;
      user.updatedAt = new Date() ;
      if (!user.api_keys) user.api_keys = JSON.stringify([]) ;
      return user;
    })

    /*crypto.randomBytes(16, (err, buf) => {
      if (err) throw err;
      console.log(`${buf.length} bytes of random data: ${buf.toString('hex')}`);
    });*/
    console.log(finalized);
    return queryInterface.bulkInsert('Teachers',finalized);
  },
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Teachers', null, {});
  }
};
