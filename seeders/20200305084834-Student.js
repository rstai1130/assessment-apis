'use strict';

const crypto = require('crypto');

module.exports = {
  up: (queryInterface, Sequelize) => {

    var user_arr = [      
      {
        email: 'studentjon@example.com',
      },      
      {
        email: 'studenthon@example.com',
      },      
      {
        email: 'commonstudent1@gmail.com',
      },      
      {
        email: 'commonstudent2@gmail.com',
      },
      {
        email: 'student_only_under_teacher_ken@gmail.com',
      },      
      {
        email: 'studentmary@gmail.com',
      },      
      {
        email: 'studentagnes@example.com',
      },      
      {
        email: 'studentmiche@example.com',
      },      
      {
        email: 'studentbob@example.com',
      },
    ];

    var finalized = user_arr.map((user)=> {
      user.createdAt = new Date() ;
      user.updatedAt = new Date() ;
      if (!user.api_keys) user.api_keys = JSON.stringify([]) ;
      return user;
    })

    return queryInterface.bulkInsert('Students',finalized);
  },
  down: (queryInterface, Sequelize) => {
      return queryInterface.bulkDelete('Students', null, {});
  }
};
