
const request = require('supertest');
const app = require('../../server');

describe("Test Notification", () => {
	it("test with @non-active-student", async done => {
		var res = 
			await request(app)
			.post('/api/retrievefornotifications')
			.send({ 
				"teacher" : "teacherken@gmail.com" , 
				"notification" : "Hello students! @rstai@example.com"
			})
			.set('Accept', 'application/json')
			.expect(400)
			.expect( (res) => {
				if( !res.body.message || (res.body.message.error !== "No active record found for rstai@example.com" ) ) 
					throw new Error(`Unrecognized message : ${JSON.stringify(res.body.message)}`);
			})
			done();
	},10000);		
	it("test normal with @active-student", async done => {
		var res = 
			await request(app)
			.post('/api/retrievefornotifications')
			.send({ 
				"teacher" : "teacherken@gmail.com" , 
				"notification" : "Hello students! @studentbob@example.com"
			})
			.set('Accept', 'application/json')
			.expect(200)
			.expect( (res) => {
				if( res.body.message ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);		
	it("test normal without @", async done => {
		var res = 
			await request(app)
			.post('/api/retrievefornotifications')
			.send({ 
				"teacher" : "teacherken@gmail.com" , 
				"notification" : "Hello everybody! "
			})
			.set('Accept', 'application/json')
			.expect(200)
			.expect( (res) => {
				if( !res.body.recipients || !res.body.recipients.length ) 
					throw new Error(`No recipients found`);
			})
			done();
	},10000);		
	it("test @non-email", async done => {
		var res = 
			await request(app)
			.post('/api/retrievefornotifications')
			.send({ 
				"teacher" : "teacherken@gmail.com" , 
				"notification" : "Hello students! @studentagnes@example.com @studentmiche@example.com @thisisnotemail"
			})
			.set('Accept', 'application/json')
			.expect(200)
			.expect( (res) => {
				if( res.body.message ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	
	it("test no body", async done => {
		var res = 
			await request(app)
			.post('/api/retrievefornotifications')
			.send(null)
			.set('Accept', 'application/json')
			.expect(400)
			.expect( (res) => {
				if( !res.body.message || (res.body.message !== "Please define notification sender (key:teacher) and content (key:notification)" )) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	
})