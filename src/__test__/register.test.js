const request = require('supertest');
const app = require('../../server');

var newteacher = 'teacher_test_by_register_1@example.com';
var existteacher = 'teacherken@gmail.com';
var newstudents = ["stud3_by_register@example.com" , "stud4_by_register@example.com" ] ;
var existstudents = ["studentmary@gmail.com" ] ;

describe("Test Register", () => {
	it("test register ken", async done => {
		var res = 
			await request(app)
			.post('/api/register')
			.send({ 
				"teacher" : existteacher, 
				"students" : existstudents
			})
			.set('Accept', 'application/json')
			.expect(202)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "OK") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	
	it("test register and create all", async done => {
		var res = 
			await request(app)
			.post('/api/register')
			.send({ 
				"teacher" : newteacher, 
				"students" : newstudents
			})
			.set('Accept', 'application/json')
			.expect(202)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "OK") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	

	it("test no teacher", async done => {
		var res = 
			await request(app)
			.post('/api/register')
			.send({ 
				"teacher" : null , 
				"students" : newstudents
			})
			.set('Accept', 'application/json')
			.expect(400)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "Please define teacher to registered under") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	

	it("test no students", async done => {
		var res = 
			await request(app)
			.post('/api/register')
			.send({ 
				"teacher" : existteacher, 
				"students" : [] 
			})
			.set('Accept', 'application/json')
			.expect(400)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "Please define students to be registered") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);
})