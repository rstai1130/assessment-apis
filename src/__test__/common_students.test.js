
const request = require('supertest');
const app = require('../../server');


describe("Test Common Students", () => {
	it("test one teacher with students", async done => {
		var res = 
			await request(app)
			.get('/api/commonstudents?teacher=teacherken%40gmail.com')
			.set('Accept', 'application/json')
			.expect(200)
			.expect( (res) => {
				if( res.body.message ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
				if( (!res.body.students.length) ) 
					throw new Error(`No students returned`);
			})
			done();
	},10000);		
	it("test more than 1 teacher", async done => {
		var res = 
			await request(app)
			.get('/api/commonstudents?teacher=teacherken%40gmail.com&teacher=teacherjoe%40gmail.com')
			.set('Accept', 'application/json')
			.expect(200)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.students.length) ) 
					throw new Error(`No students returned`);
			})
			done();
	},10000);	

	it("test invalid teacher", async done => {
		var res = 
			await request(app)
			.get('/api/commonstudents?teacher=teacherken%40gmail.com&teacher=invalidteacher%40gmail.com')
			.set('Accept', 'application/json')
			.expect(400)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message.error !== "Records not found for invalidteacher@gmail.com") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	

	it("test no common", async done => {
		var res = 
			await request(app)
			.get('/api/commonstudents?teacher=teacherken%40gmail.com&teacher=new_teacher_by_register%40gmail.com')
			.set('Accept', 'application/json')
			.expect(200)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( res.body.students.length ) 
					throw new Error(`Found unexpected common students: ${res.body.students}`);
			})
			done();
	},10000);
})