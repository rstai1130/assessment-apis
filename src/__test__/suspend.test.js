const request = require('supertest');
const app = require('../../server');

var newteacher = 'teahcer_suspend@example.com';
var newstudents = ["test_suspend_3@example.com" ,"test_suspend_4@example.com"] ;
var invalidstudent = 'rstai1130@example.com' ;

describe("Test Suspend", () => {
	it("Suspend one", async done => {
		await request(app)
			.post('/api/register')
			.send({ 
				"teacher" : newteacher , 
				"students" : newstudents
				
			})
			.set('Accept', 'application/json')
			.expect(202)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "OK") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
		var res = 
			await request(app)
			.post('/api/suspend')
			.send({ 
				"student" : newstudents[0] , 
			})
			.set('Accept', 'application/json')
			.expect(204)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (res.body.message) ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	

	it("Invalid student", async done => {
		var res = 
			await request(app)
			.post('/api/suspend')
			.send({ 
				"student" : "rstai1130@example.com" , 
			})
			.set('Accept', 'application/json')
			.expect(404)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "Record not found for rstai1130@example.com") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);		

	it("No student", async done => {
		var res = 
			await request(app)
			.post('/api/suspend')
			.send({ 
				"student" : null , 
			})
			.set('Accept', 'application/json')
			.expect(400)
			.expect( (res) => {
				//console.log(typeof res.body);
				if( (!res.body.message) || (res.body.message !== "Please define email of student to be suspended") ) 
					throw new Error(`Unrecognized message : ${res.body.message}`);
			})
			done();
	},10000);	


})