'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable(
      'registers',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        teacherid:  { 
          type : Sequelize.INTEGER,
          references: {
              model: 'Teachers',
              key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade',
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE
        },
        updatedAt: {
          type: Sequelize.DATE
        },
        studentid:  { 
          type : Sequelize.INTEGER,
           references: {
              model: 'Students',
              key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade',
          allowNull: false
        },        
      }
    )
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('registers');
  }
};
