'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    
    return queryInterface.createTable(
      'teachers',
      {
        id: {
          type: Sequelize.INTEGER,
          primaryKey: true,
          autoIncrement: true
        },
        email:  { 
          type : Sequelize.STRING,
          allowNull: false
        },
        createdAt: {
          type: Sequelize.DATE
        },
        updatedAt: {
          type: Sequelize.DATE
        },     
        api_keys:  { 
          type : Sequelize.JSON,
          defaultValue: [],
          allowNull: false
        },        
        status:  { 
          type : Sequelize.STRING,
          defaultValue: 'active',
          allowNull: false
        }

      }
    )
  },

  down: (queryInterface, Sequelize) => {
      return queryInterface.dropTable('teacher');
  }
};
