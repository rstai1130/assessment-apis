'use strict';
module.exports = (sequelize,DataTypes) => {
  const student = sequelize.define('Students', {
    email:  { 
      type : DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },        
    api_keys:  { 
      type : DataTypes.JSON,
      defaultValue: [],
      allowNull: false
    },        
    status:  { 
      type : DataTypes.STRING,
      defaultValue: 'active',
      allowNull: false
    }
  }, {
  });
  student.associate = (models) => {
    models.Students.hasMany(models.Registers,{ foreignKey : 'studentid' });
  }
  return student;
}
