'use strict';
module.exports = (sequelize,DataTypes) => {
  const register = sequelize.define('Registers', {
    // Model attributes are defined here
      teacherid:  { 
        type : DataTypes.INTEGER,
        references: {
            model: 'Teacher',
            key: 'id'
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
        allowNull: false
      },
      createdAt: {
        type: DataTypes.DATE
      },
      updatedAt: {
        type: DataTypes.DATE
      },
      studentid:  { 
          type : DataTypes.INTEGER,
           references: {
              model: 'Student',
              key: 'id'
          },
          onUpdate: 'cascade',
          onDelete: 'cascade',
          allowNull: false
        },   
  }, {
    // Other model options go here
  });
  register.associate = (models) => {
    models.Registers.belongsTo(models.Teachers,{ foreignKey : 'id' });
    models.Registers.belongsTo(models.Students,{ foreignKey : 'id' });
    //models.Register.belongsTo(models.User,{ foreignKey : 'studentid' });
  }
  return register;
}