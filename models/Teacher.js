'use strict';
module.exports = (sequelize,DataTypes) => {
  const teacher = sequelize.define('Teachers', {
    email:  { 
      type : DataTypes.STRING,
      allowNull: false
    },
    createdAt: {
      type: DataTypes.DATE
    },
    updatedAt: {
      type: DataTypes.DATE
    },       
    api_keys:  { 
      type : DataTypes.JSON,
      defaultValue: JSON.stringify([ { teacher : 'some_hashed_salted_general_keys' }]) ,
      allowNull: false
    },        
    status:  { 
      type : DataTypes.STRING,
      defaultValue: 'active',
      allowNull: false
    }
  });
  teacher.associate = (models) => {
    models.Teachers.hasMany(models.Registers,{ foreignKey : 'teacherid' });
  }
  return teacher;
}
