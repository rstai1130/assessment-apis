Main packages used :
================================
server : ExpressJS
db : mysql
ORM : sequelize
Unit testing : JEST and supertest



Working instruction 
================================

Setup in your local : 

1) Setup your mysql server ( with root and password )
2) Pull from this repo
3) update environment variable for mysql database in config/config.json and config/database.js
4) use command : sequelize db:migrate (seed db if you want to)
5) initialize server command : nodemon server 
6) run your api test

For unit test :

1) Modify test cases in src/__test__ folder to your preferences
2) comment out 'app.listen(...' in /server.js to prevent timeout
3) use command : npm test <optional : test-file-path> to run test 
