const models = require('../models');

exports.allStud = async (req,res) => {
	const records = await models.Students.findAll({ include:models.Registers });
	return res.send(records);	
} 

exports.allTeach = async (req,res) => {
	const records = await models.Teachers.findAll({ include:models.Registers });
	return res.send(records);	
} 

// for suspending student
exports.suspendStudent = async (req,res) => {
	try {
		//return res.status(202).send({ message : req.body.student });	
		if (!req.body || !req.body.student) return res.status(400).send({ message : `Please define email of student to be suspended` });	

		const  [ update , updated ] = await models.Students.update({ status : 'suspend' }, {
			where : {
				email : req.body.student
			}
		})
		//return res.status(202).send({ message : !!update });	
		if (update) {
			return res.status(204).send({ message : 'OK' });	
		} else {
			return res.status(404).send({ message : `Record not found for ${req.body.student}` });	
		}

	} catch (err) {

		return res.status(500).send({ message : err });	

	}
} 

// remove suspend
exports.unsuspendStudent = async (req,res) => {
	
} 

