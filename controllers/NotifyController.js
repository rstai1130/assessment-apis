const models = require('../models');
const service = require('./Service');


exports.getNotified = async (req,res) => {
	
	if ( !req.body || !req.body.teacher || !req.body.notification ) {
		return service.badRequest(res,
			"Please define notification sender (key:teacher) and content (key:notification)"
			);
	}

	const mentioned = await this.getMentioned(req.body.notification);
	
	var mentionIDs = [];

	if (mentioned.length) {
		
		mentionIDs = await this.getStudentIds( mentioned.map((tagged)=> tagged.substring(1)) );

		const checkError = await service.hasError(mentionIDs);

		if (checkError.length) return service.badRequest(res,checkError);

	}

	const registered = await service.getTeacherStudentList([req.body.teacher]);	

	const checkError2 = await service.hasError(registered);

	if (checkError2.length) return service.badRequest(res,checkError2);

	const registeredIDs = registered.map((reg)=> { return reg.Registers}).flat(1).map((aa)=> { return aa.studentid }) ;

	const consolidated = [...new Set( registeredIDs.concat(mentionIDs.map( mention=> { return mention.id } )) )];

	const recip = await service.getStudentEmails(consolidated);

	const emails = recip.map( (rec) => { return rec.email } ); 

	//console.log(` ${JSON.stringify(req.body)} :: ${JSON.stringify(emails)}`);

	return res.status(200).send({ recipients : emails });

}

exports.getMentioned = async (content) => {
	
	const emailFormat = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i

	return await content.split(' ').filter((word)=> {

		return word.startsWith('@') && emailFormat.test( word.substring(1) );

	})

}

exports.getStudentIds = async (emails) => {

	return Promise.all( emails.map( async(mail) => {

		return await models.Students.findOne( {

			attributes : ['id'],
			where : { email : mail , status : 'active' } 

		}  ) || Promise.resolve( { error : `No active record found for ${mail}`} );
	
	}) ) 
}