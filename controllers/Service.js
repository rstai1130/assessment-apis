/*
	
	This is for methods shared across different controllers 
	
*/

const models = require('../models');


//create student/teacher user if doesnt no exist
exports.createIfNotExist = async (user_arr,model) => {
	console.log('createIfNotExist');
	return Promise.all( user_arr.map( async (user)=> {

		const [ target , created ] = await model.findOrCreate( {
			where :  { email : user }
		} )

		return Promise.resolve(target.id);

	}))

}

//get student emails by ID
exports.getStudentEmails = async (ids) => {

	return Promise.all( ids.map( async(id) => {

		return await models.Students.findOne( {

			attributes : ['email'],
			where : { id : id } 

		} )
	
	}) );
}


exports.getTeacherStudentList = async (emails) => {

	return Promise.all( emails.map( async (tmail) => {

		return await models.Teachers.findOne({ 

			attributes : ['email'],
			where : { email : tmail },
			include : [ { model : models.Registers , attributes : ['studentid'] }]

		}) || Promise.resolve({ error : `Records not found for ${tmail}` });
	
	}) );
}


exports.hasError = async (result) => {

	const getError = result.filter( res => { return res.error });

	return  (getError.length) ? getError : false;

};

exports.badRequest = async (res,msg="Bad Request") => {
	

	var err_msg =  (typeof msg == 'object' && msg.length == 1) ? msg[0] : msg;
	
	return res.status(400).send({ message : err_msg });

};