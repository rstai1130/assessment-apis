const models = require('../models');
const service = require('./Service');


exports.allregister = async (req,res) => {
	return res.send(await models.Registers.findAll())	
} 

exports.register = async (req,res) => {

	//console.log(req.body);
	if (!req.body.teacher) 
		return res.status(400).send({ message : "Please define teacher to registered under" });

	if (!req.body.students || !req.body.students.length) 
		return res.status(400).send({ message : "Please define students to be registered" });

	const [ teacherids , studentids ] = await Promise.all( [
		service.createIfNotExist([req.body.teacher],models.Teachers).then((resolve)=> { return resolve; })
		,
		service.createIfNotExist(req.body.students,models.Students).then((resolve)=> { return resolve; })
	])

	return await this.createOrUpdateRegister(teacherids,studentids).then((resolve)=> {

		return res.status(202).json({ message : "OK" });

	});
	
}



//create or update for registering student to teacher
exports.createOrUpdateRegister = async (teachers,students) => {

	return Promise.all( teachers.map ( async (teacher) => {

		return Promise.all( students.map ( async (stud) => {

			return await models.Registers.findOrCreate( {
				where :  { teacherid : teacher , studentid : stud }
			} )

		}));

	})); 
	
}

//find common students between list of teachers
exports.commonStudents = async (req,res) => {
	try  {
		var teacher_arr = (typeof req.query.teacher == 'string') ? [req.query.teacher] : req.query.teacher ;
		
		const teacherStudents = await service.getTeacherStudentList( teacher_arr ) ;
		
		if (!teacherStudents) {

			return res.status(400).send({ message : "No registered students found" });

		} else {

			const checkError = await service.hasError(teacherStudents);

			if (checkError.length) return service.badRequest(res,checkError);

			const groups = await Promise.all( teacherStudents.map( async (ts)=> {
				return ts.Registers.map((reg)=> {
					return reg.studentid;
				})
			}));

			let final_arr = [];
			
			if (groups.length>1) {
				for ( i = 0 ; i <= groups.length-2 ; i++ ) {
					//console.log(` ${groups[i]} vs ${groups[i+1]}`);
					final_arr = groups[i].filter( stud => { 
						return groups[i+1]?groups[i+1].includes(stud):true; 
					});
				}
			} else {
				final_arr = groups[0];
			}

			//console.log(` Students register :: ${JSON.stringify(groups)} `);
			//console.log(` Common Students :: ${JSON.stringify(final_arr)} `);
			const result = await service.getStudentEmails(final_arr);
			return res.status(200).send({ students : result.map( (stud) => { return stud.email }) });

		}

	} catch (err) {
		console.log(err);

		return (!req.query || !req.query.teacher)?
			res.status(400).send({ message : "Please define teacher to look up for registered students" })
			: res.status(500).send({ message : err });

	}
	
}



