const express = require('express');
const router = express.Router();
const userController = require('../controllers/UserController');
const registerController = require('../controllers/RegisterController');
const notifyController = require('../controllers/NotifyController');

router.get('/',async (req,res)=> {	res.send('Welcome to Ufinity APIs');  })

/*router.get('/allstud',	userController.allStud	);

router.get('/allteach',	userController.allTeach	);*/

//As a teacher, I want to register one or more students to a specified teacher.
router.post('/register', registerController.register )

//As a teacher, I want to retrieve a list of students common to a given list of teachers (i.e. retrieve students who are registered to ALL of the given teachers).
router.get('/commonstudents', registerController.commonStudents )

 //As a teacher, I want to suspend a specified student.
router.post('/suspend',	userController.suspendStudent )

// As a teacher, I want to retrieve a list of students who can receive a given notification.
router.post('/retrievefornotifications', notifyController.getNotified )

module.exports = router ;